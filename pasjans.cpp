// pasjans.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "talia.h"
#include "plansza.h"
#include "Gra.h"
#include <conio.h>
#include <windows.h>

int main()
{

	COORD c = { 1000, 1000 };
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), c);
	//ShowWindow(GetStdHandle(STD_OUTPUT_HANDLE), SW_MAXIMIZE);

	unsigned char znak = 'a';
	Gra *gra = new Gra();
	Plansza *plan = new Plansza(gra);

	do
	{
		while (_kbhit())
		{
			znak = _getch();
			plan->klawisze(znak);
		}

	} while (znak != 27); //ESC

	system("PAUSE");
	return 0;
}


