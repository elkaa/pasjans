#pragma once
#include "stdafx.h"
#include "talia.h"

enum RTalie
{
	TALIA_1 = 0,
	TALIA_2 = 1,
	TALIA_3 = 2,
	TALIA_4 = 3,
	TALIA_5 = 4,
	TALIA_6 = 5,
	TALIA_7 = 6,
	TALIA_8 = 7,
	TALIA_S1 = 8,
	TALIA_S2 = 9,
	TALIA_S3 = 10,
	TALIA_S4 = 11,
	TALIA_LAST = 12
};

class Gra
{
public:
	Gra();
	virtual ~Gra();
	Talia *zwrocTalie(RTalie rt);
	bool wieksza(string st, string k);
	bool mniejszaWieksza(string st, string k);
	short int pokazWynik();
	void resetujGre();
	void dodajPunkty(short int p);
	void odejmijPunkty(short int p);

protected:
	void utworzTalieKart();


private:
	Talia *T[14];
	short int wynik;
};
