#include "stdafx.h"
#include "Gra.h"

Gra::Gra()
{
	utworzTalieKart();
	wynik = 0;
}

Gra::~Gra()
{

}

void Gra::utworzTalieKart()
{
	//Utworzenie obietkow
	for (int i = TALIA_1; i < TALIA_LAST + 1; i++)
		T[i] = new Talia();

	//Tworzenie glownej talii i tasowanie
	T[TALIA_LAST]->utworzTalie();
	T[TALIA_LAST]->tasujTalie();

	//Rozdanie kart
	for (int i = TALIA_1; i < TALIA_S1; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			Karta *karta = T[TALIA_LAST]->wysunKarte();
			T[i]->dodajNaPoczatek(karta);
		}
	}
	Karta *k;
	//Dodawanie asow na stosy
	for (int i = TALIA_S1; i < TALIA_LAST; i++)
	{
		k = new Karta;
		k->nazwa = "A";
		k->kolor = T[1]->kolory(i % 4);
		T[i]->dodajNaPoczatek(k);
	}


}

Talia *Gra::zwrocTalie(RTalie rt)
{
	return T[rt];
}

bool Gra::wieksza(string st, string k)
{
	short int wartoscS = -1;
	short int wartoscK = -1;

	if (st.empty() || k.empty())
		return false;

	for (int i = 0; i < 13; i++)
	{
		if (st == T[0]->nazwy(i))
			wartoscS = i;
		else if (k == T[0]->nazwy(i))
			wartoscK = i;
	}

	return ((wartoscS != -1) && (wartoscK != -1) && (wartoscS - 1 == wartoscK));

}

bool Gra::mniejszaWieksza(string st, string k)
{
	short int wartoscS = -1;
	short int wartoscK = -1;

	if (st.empty() || k.empty())
		return false;

	for (int i = 0; i < 13; i++)
	{
		if (st == T[0]->nazwy(i))
			wartoscS = i;
		else if (k == T[0]->nazwy(i))
			wartoscK = i;
	}

	return ((wartoscS != -1) && (wartoscK != -1) && ((wartoscS - 1 == wartoscK) || (wartoscS + 1 == wartoscK)));
}


short int Gra::pokazWynik()
{
	return wynik;
}

void Gra::dodajPunkty(short int p)
{
	wynik += p;
}
void Gra::odejmijPunkty(short int p)
{
	wynik -= p;
}


void Gra::resetujGre()
{
	wynik = 0;

	for (int i = TALIA_1; i < TALIA_LAST + 1; i++)
		delete T[i];

	utworzTalieKart();

}
