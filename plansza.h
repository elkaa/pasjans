#pragma once
#include "stdafx.h"
#include "Gra.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include <conio.h>
#include <sstream>
#include <vector>

using namespace std;

enum POZYCJA
{
	K1,
	K2,
	K3,
	K4,
	K5,
	K6,
	K7,
	K8,
	STOS1,
	STOS2,
	STOS3,
	STOS4

};

enum STATUS
{
	MENU,
	GRA,
	AUTORZY,
	KONIEC
};

struct Punkt
{
	int x;
	int y;
};

class Plansza
{
public:
	Plansza(Gra *g);
	~Plansza();

	void rysujMenu(bool przerysuj);
	void rysujGre(bool przerysuj);
	void rysujAutorzy(bool przerysuj);
	void koniecGry();
	bool czyWygrana();
	int oznacz(bool &a, int b);
	void klawisze(unsigned char znak);
	STATUS status();
	template <typename T> string inttostr(T n);

private:
	void rysujKarte(int x, int y, string text, int text2, int kolor);
	void rysujZnak(int ax, int ay, string text, int kolor);
	bool rozneOdStosow(int x, int y);
	void zmienStatus(STATUS st);
	string wczytajPlik(string nazwa);
	Punkt pozycjaKarty(POZYCJA slot);
	STATUS statusPlanszy;
	short int menuPos;
	Punkt planszaMenuPos;
	short int iloscOpcjiMenu;
	short int planszaPos;
	Punkt wybrana;
	bool wyb;
	Gra *gra;


};

